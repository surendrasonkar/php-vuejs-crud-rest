<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP| MySQL | Vue.js | Axios Example</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
   
</head>
<div class="container" id='vueapp'>
<div class="row">
	<h1>Contact Management</h1>
<table class="table table-striped table-bordered"> 
		<thead> 
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Country</th>
            <th>City</th>
            <th>Job</th>
        </tr> 
        </thead>
        <tbody>
        <tr v-for='contact in contacts'>
            <td>{{ contact.name }}</td>
            <td>{{ contact.email }}</td>
            <td>{{ contact.country }}</td>
            <td>{{ contact.city }}</td>
            <td>{{ contact.job }}</td>
        </tr>
        </tbody>
     </table>
    </div>
<br/>
    <h1>Add New Contact</h1>
    <div class="row">
		<form class="form-horizontal col-md-6 col-md-offset-3">			         
			<div class="form-group">
			    <label for="input1" class="col-sm-2 control-label">Name</label>
			    <div class="col-sm-10">
			      <input type="text" name="name" v-model="name" class="form-control" id="input1" placeholder="Name" />
			    </div>
			</div>

            <div class="form-group">
			    <label for="input1" class="col-sm-2 control-label">Email</label>
			    <div class="col-sm-10">
			      <input type="email" name="email" v-model="email" class="form-control" id="input1" placeholder="Email Address" />
			    </div>
			</div>

            <div class="form-group">
			    <label for="input1" class="col-sm-2 control-label">Country</label>
			    <div class="col-sm-10">
			      <input type="text" name="country" v-model="country" class="form-control" id="input1" placeholder="Country" />
			    </div>
			</div>


            <div class="form-group">
			    <label for="input1" class="col-sm-2 control-label">City</label>
			    <div class="col-sm-10">
			      <input type="text" name="city" v-model="city" class="form-control" id="input1" placeholder="City" />
			    </div>
			</div>

            <div class="form-group">
			    <label for="input1" class="col-sm-2 control-label">Job</label>
			    <div class="col-sm-10">
			      <input type="text" name="job" v-model="job" class="form-control" id="input1" placeholder="Job" />
			    </div>
			</div>
            <input type="button" class="btn btn-primary col-md-2 col-md-offset-10" @click="createContact()" value="Add" />
        </form>
    </div>
</div>
<script>
var app = new Vue({
  el: '#vueapp',
  data: {
      name: '',
      email: '',
      country: '',
      city: '',
      job: '',
      contacts: []
  },
  mounted: function () {
    console.log('Hello from Vue!')
    this.getContacts()
  },

  methods: {
    getContacts: function(){
        axios.get('api/contacts.php')
        .then(function (response) {
            console.log(response.data);
            app.contacts = response.data;

        })
        .catch(function (error) {
            console.log(error);
        });
    },
    createContact: function(){
        console.log("Create contact!")

        let formData = new FormData();
        console.log("name:", this.name)
        formData.append('name', this.name)
        formData.append('email', this.email)
        formData.append('city', this.city)
        formData.append('country', this.country)
        formData.append('job', this.job)

        var contact = {};
        formData.forEach(function(value, key){
            contact[key] = value;
        });

        axios({
            method: 'post',
            url: 'api/contacts.php',
            data: formData,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        })
        .then(function (response) {
            //handle success
            console.log(response)
            app.contacts.push(contact)
            app.resetForm();
        })
        .catch(function (response) {
            //handle error
            console.log(response)
        });
    },
    resetForm: function(){
        this.name = '';
        this.email = '';
        this.country = '';
        this.city = '';
        this.job = '';
    }
  }
})    
</script>

<body>
</body>
</html>

